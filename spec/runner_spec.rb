RSpec.describe Routes::Runner do
  let(:input_string) { 'AB5, BC4, CD8, DC8, DE6, AD5, CE2, EB3, AE7' }

  subject { described_class.new(input_string) }

  describe '#example_1' do
    it { expect(subject.example_1).to eq 9 }
  end

  describe '#example_2' do
    it { expect(subject.example_2).to eq 5 }
  end

  describe '#example_3' do
    it { expect(subject.example_3).to eq 13 }
  end

  describe '#example_4' do
    it { expect(subject.example_4).to eq 22 }
  end

  describe '#example_5' do
    it { expect(subject.example_5).to eq 'NO SUCH ROUTE' }
  end

  describe '#example_6' do
    it { expect(subject.example_6).to eq 2 }
  end

  describe '#example_7' do
    it { expect(subject.example_7).to eq 3 }
  end

  describe '#example_8' do
    it { expect(subject.example_8).to eq 9 }
  end

  describe '#example_9' do
    it { expect(subject.example_9).to eq 9 }
  end

  describe '#example_10' do
    it { expect(subject.example_10).to eq 6 }
  end
end
