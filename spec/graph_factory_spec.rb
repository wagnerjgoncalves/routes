RSpec.describe Routes::GraphFactory do
  describe 'without a valid input string' do
    it 'raise an error' do
      expect { described_class.new(nil).output }.to raise_error(Routes::Error)
    end
  end

  describe 'with a valid input string' do
    let(:input_string) { 'AB5, BC4, CD8, DC8, DE6, AD5, CE2, EB3, AE7' }

    it { expect(described_class.new(input_string).call).to_not be_nil }

    it { expect(described_class.new(input_string).nodes).to_not be_nil }
  end
end
