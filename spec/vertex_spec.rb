require 'spec_helper'

RSpec.describe Routes::Vertex do
  let(:a) { Routes::Node.new('A') }
  let(:b) { Routes::Node.new('B') }

  describe '#to_s' do
    subject { described_class.new(a, b, 5) }

    it { expect(subject.to_s).to eq 'AB5' }
  end
end
