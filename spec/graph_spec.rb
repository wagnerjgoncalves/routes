require 'spec_helper'

RSpec.describe Routes::Graph do
  let(:node_a) { Routes::Node.new('A') }
  let(:node_b) { Routes::Node.new('B') }
  let(:node_c) { Routes::Node.new('C') }
  let(:node_d) { Routes::Node.new('D') }

  it { expect(subject.vertexs).to be_empty }
  it { expect(subject.nodes).to be_empty }

  describe '#add_vertex' do
    it 'change veterxs by 1' do
      expect do
        subject.add_vertex(Routes::Vertex.new(node_a, node_b, 10))
      end.to change { subject.vertexs.length }.by(1)
    end
  end

  describe '#find_adjacent_vertexs_from' do
    let(:vertex_a_b) { Routes::Vertex.new(node_a, node_b, 10) }
    let(:vertex_b_c) { Routes::Vertex.new(node_b, node_c, 40) }
    let(:vertex_a_c) { Routes::Vertex.new(node_a, node_c, 35) }
    let(:vertex_d_c) { Routes::Vertex.new(node_d, node_c, 15) }

    before do
      subject.add_vertex(vertex_a_b)
      subject.add_vertex(vertex_b_c)
      subject.add_vertex(vertex_a_c)
      subject.add_vertex(vertex_d_c)
    end

    it 'return adjacent vertexs from a' do
      expected_values = [vertex_a_b, vertex_a_c]

      expect(subject.find_adjacent_vertexs_from(node_a)).to eq expected_values
    end

    it 'return adjacent vertexs from b' do
      expect(subject.find_adjacent_vertexs_from(node_b)).to eq [vertex_b_c]
    end

    it 'return none to adjacent vertexs from c' do
      expect(subject.find_adjacent_vertexs_from(node_c)).to be_empty
    end
  end

  describe '#nodes' do
    before do
      subject.add_vertex(Routes::Vertex.new(node_a, node_b, 10))
      subject.add_vertex(Routes::Vertex.new(node_b, node_c, 20))
      subject.add_vertex(Routes::Vertex.new(node_a, node_c, 40))
      subject.add_vertex(Routes::Vertex.new(node_c, node_d, 5))
    end

    it 'return uniq nodes' do
      expect(subject.nodes).to eq [node_a, node_b, node_c, node_d]
    end
  end
end
