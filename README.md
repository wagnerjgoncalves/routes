# Routes

Routes is a Ruby Gem with the resposability to solve the trains test.
The purpose is to help the railroad provide its customers with information
about the routes.

## Installation

And then execute:

    $ bin/setup

## Usage

There's a rake task to print the output of the test example to the input:

```
AB5, BC4, CD8, DC8, DE6, AD5, CE2, EB3, AE7
```

    $ rake output


### Running tests

    $ rake spec

### Running rubocop

    $ rake rubocop
