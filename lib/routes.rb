require 'routes/graph'
require 'routes/graph_factory'
require 'routes/node'
require 'routes/route'
require 'routes/runner'
require 'routes/version'
require 'routes/vertex'

module Routes
  class Error < StandardError; end
end
