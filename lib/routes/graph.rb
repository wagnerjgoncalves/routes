module Routes
  class Graph
    attr_accessor :vertexs

    def initialize
      @nodes = []
      @vertexs = []
    end

    def add_vertex(vertex)
      vertexs << vertex
    end

    def find_adjacent_vertexs_from(node)
      vertexs.select { |e| e.from == node }
    end

    def nodes
      return [] if vertexs.empty?

      @nodes = vertexs.collect(&:from) + vertexs.collect(&:to)

      @nodes.uniq!(&:name)
    end
  end
end
