module Routes
  class Vertex
    attr_accessor :from, :to, :distance

    def initialize(from, to, distance)
      @from = from
      @to = to
      @distance = distance
    end

    def to_s
      "#{from}#{to}#{distance}"
    end
  end
end
