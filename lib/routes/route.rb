module Routes
  class Route
    attr_accessor :graph, :from, :to, :routes

    def initialize(graph, from, to)
      @graph = graph
      @from = from
      @to = to
      @routes ||= {}

      generate_routes(from.to_s, from, 0)
    end

    def ends_to
      routes.select { |route, _| route.end_with?(to.to_s) }
    end

    def with_up_to_stops(stops)
      ends_to.select { |route, _| (route.size - 1) <= stops }
    end

    def with_exactly_stops(stops)
      ends_to.select { |route, _| (route.size - 1) == stops }
    end

    def with_distance(distance)
      ends_to.select { |_, d| d < distance }
    end

    private

    def generate_routes(route_prefix, current_node, current_distance)
      vertexes = graph.find_adjacent_vertexs_from(current_node)

      return if vertexes.empty?

      vertexes.each do |vertex|
        break if vertex_already_processed?(route_prefix, vertex)

        new_route_prefix = "#{route_prefix}#{vertex.to}"
        new_distance = current_distance + vertex.distance

        routes[new_route_prefix] = new_distance

        generate_routes(new_route_prefix, vertex.to, new_distance)
      end
    end

    def vertex_already_processed?(route_prefix, vertex)
      route_prefix.scan(vertex.from.to_s + vertex.to.to_s).count > 1
    end
  end
end
