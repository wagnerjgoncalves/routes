module Routes
  class GraphFactory
    attr_accessor :routes, :nodes

    def initialize(input)
      @routes = extract_routes(input)
      @nodes = {}
    end

    def call
      instantiate_graph
    end

    private

    def create_vertex(route)
      route_array = route.split('')

      from = find_or_create_node(route_array[0])
      to = find_or_create_node(route_array[1])
      distance = route.slice(2, route.size).to_i

      Routes::Vertex.new(from, to, distance)
    end

    def extract_routes(input)
      raise Routes::Error, 'Input invalid' unless input

      input.split(',').collect(&:strip)
    end

    def find_or_create_node(name)
      nodes[name] = Routes::Node.new(name) unless nodes[name]
      nodes[name]
    end

    def instantiate_graph
      graph = Routes::Graph.new

      routes.each do |route|
        graph.add_vertex(create_vertex(route))
      end

      graph
    end
  end
end
