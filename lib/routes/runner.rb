module Routes
  class Runner
    attr_accessor :input

    def initialize(input)
      @input = input
    end

    def output
      (1..10).each do |index|
        data = send "example_#{index}".to_sym
        puts "##{index}: #{data}"
      end
    end

    def example_1
      Routes::Route.new(graph, nodes['A'], nodes['C']).ends_to['ABC']
    end

    def example_2
      Routes::Route.new(graph, nodes['A'], nodes['D']).ends_to['AD']
    end

    def example_3
      Routes::Route.new(graph, nodes['A'], nodes['C']).ends_to['ADC']
    end

    def example_4
      Routes::Route.new(graph, nodes['A'], nodes['D']).ends_to['AEBCD']
    end

    def example_5
      response = Routes::Route.new(graph, nodes['A'], nodes['D']).ends_to['AED']
      response || 'NO SUCH ROUTE'
    end

    def example_6
      route = Routes::Route.new(graph, nodes['C'], nodes['C'])
      route.with_up_to_stops(3).count
    end

    def example_7
      route = Routes::Route.new(graph, nodes['A'], nodes['C'])
      route.with_exactly_stops(4).count
    end

    def example_8
      route = Routes::Route.new(graph, nodes['A'], nodes['C'])
      route.ends_to.min_by { |_, distance| distance }.last
    end

    def example_9
      Routes::Route.new(graph, nodes['B'], nodes['B']).ends_to.values.min
    end

    def example_10
      Routes::Route.new(graph, nodes['C'], nodes['C']).with_distance(30).count
    end

    private

    def graph_factory
      @graph_factory ||= Routes::GraphFactory.new(input)
    end

    def graph
      @graph ||= graph_factory.call
    end

    def nodes
      @nodes ||= graph_factory.nodes
    end
  end
end
