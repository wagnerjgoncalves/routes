require 'routes'

desc 'Output sample'
task :output do
  input = 'AB5, BC4, CD8, DC8, DE6, AD5, CE2, EB3, AE7'
  Routes::Runner.new(input).output
end
